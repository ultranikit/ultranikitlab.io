let imagesArray = ['2.png', '3.png', '4.png', '5.png', '6.png', '7.png', '8.png'];

let breakingNewsPostWrapper = document.querySelector('.breaking-news-posts-wrapper');

imagesArray.forEach(image => {
    breakingNewsPostWrapper.innerHTML +=
        `<div class="breaking-news-post">
                <a class="breaking-news-link" href="#">
                    <div class="breaking-news-image-wrap">
                        <img src="./img/breaking-news/${image}" alt="" class="breaking-news-image">
                        <div class="breaking-news-post-data">
                            <p class="breaking-news-data-day breaking-news-data-props">12</p>
                            <p class="breaking-news-data-month breaking-news-data-props">Fab</p>
                        </div>
                    </div>
                    <div class="breaking-news-post-details">
                        <p class="breaking-news-post-title">Amazing Image Post</p>
                        <div class="breaking-news-post-info">
                            <span class="breaking-news-postedby">By admin</span>
                            <div class="breaking-info-separator"></div>
                            <span class="breaking-news-comments">2 comment</span>
                        </div>
                    </div>
                </a>
            </div>`
});
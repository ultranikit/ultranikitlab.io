
let gridContainerClass = '.grid-gallery-wrapper';
let pathImage = './img/best-gallery/';
// let galleryContClass = document.querySelector('.best-gallery-container');
let newCreate = new BestGallery(gridContainerClass, pathImage);
newCreate.addGridLayout();

function BestGallery(gridsContainer, imagePath) {

    let gridGalleryWrap = document.querySelector(`${gridsContainer}`);
    this.addGridLayout = function () {
        // выбераем контейнер в котором будут находиться все сетки
        // добавим новую пустую сетку
        gridGalleryWrap.innerHTML +=
        `<div class="grid">
        <div class="grid-item col-1-top grid-item--height3">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-2-top grid-item--height4">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-3-top-1 grid-item--width2 grid-item--height2">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-3-top-2 grid-item--width2 grid-item--height2">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <!--row-2-->
        <div class="grid-item col-1-mid grid-item--height3">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-2-mid grid-item--height3">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--row-2  col-3  9-small-images -->
        
        <!--row-1  col-3  9-small-images -->
        <div class="grid-item col-1-small-top grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-2-small-top grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-3-small-top grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--row-2  col-3  9-small-images -->
        <div class="grid-item col-1-small-mid grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-2-small-mid grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-3-small-mid grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--row-3  col-3  9-small-images -->
        <div class="grid-item col-1-small-bottom grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-2-small-bottom grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-3-small-bottom grid-item--height1 grid-item--width1">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--row-3-->
        <div class="grid-item col-1-bottom grid-item--height3">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-2-bottom grid-item--height3">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-item col-3-bottom grid-item--height4">
            <div class="best-gallery-image-container">
                <img class="best-gallery-image" src="" alt="">
                <div class="grid-hover-effect">
                    <div class="grid-item-buttons-container">
                        <a class="grid-item-buttons">
                            <i class="fas fa-search grid-gallery-item-search"></i>
                        </a>
                        <a class="grid-item-buttons">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        </div>`;
        // инициализируем как указано в plugin masonry
        gridInit();
        loadImages();
    };
    createLoadBtn();

    function gridInit() {
// noinspection JSUnresolvedFunction
        $('.grid').masonry({
            itemSelector: '.grid-item'
        });
    }
// фунция загрузки картинок
    function loadImages() {
        let masonryImages = document.querySelectorAll('.grid-item img');
        let array = Array.from(masonryImages);
        let imageCount = 0;

        for (let i = array.length -18;i < array.length; i++) {
            array[i].setAttribute('src', `${imagePath}${imageCount}.png`);
            array[i].hidden = true;
            $(array[i]).delay(200*`${imageCount}`).fadeIn(1000);
            imageCount += 1;
        }
    }

    function createLoadBtn() {
        let btnAddImage = document.createElement('button');

        btnAddImage.id = 'masonry-load-btn';
        btnAddImage.classList.add('btn-load-images');
        btnAddImage.innerHTML = `<i class="fas fa-plus"></i><span style="margin-left: 15px">Load more</span>`;

        gridGalleryWrap.after(btnAddImage);
        loadButtonListener();
    }

    function loadButtonListener() {
        let loadBtn = document.querySelector('#masonry-load-btn');
        let counter = 0;
        let loadAnimation = document.querySelector('#load-image-gallery');

        loadBtn.addEventListener('click', ()=> {
            counter += 1;
            if (counter === 2) {
                loadBtn.remove();
            }
            loadBtn.hidden = true;
            loadAnimation.style.display = 'block';

            setTimeout(() => {
                loadAnimation.style.display = 'none';
                loadBtn.hidden = false;
                newCreate.addGridLayout();
            }, 2000);
        })
    }
}




const imagesObject = { // Обьект с картинками
    all: { 'Graphic Design': ['graphic-design1.jpg', 'graphic-design2.jpg', 'graphic-design3.jpg', 'graphic-design4.jpg',
        'graphic-design5.jpg', 'graphic-design6.jpg', 'graphic-design7.jpg', 'graphic-design8.jpg', 'graphic-design9.jpg',
        'graphic-design10.jpg', 'graphic-design11.jpg', 'graphic-design12.jpg', 'graphic-design13.jpg'],

        'Web Design': ['web-design1.jpg', 'web-design2.jpg', 'web-design3.jpg', 'web-design4.jpg', 'web-design5.jpg',
        'web-design6.jpg', 'web-design7.jpg'],

        'Landing Pages': ['landing-page1.jpg', 'landing-page2.jpg', 'landing-page3.jpg', 'landing-page4.jpg',
        'landing-page5.jpg', 'landing-page6.jpg', 'landing-page7.jpg'],

        'Wordpress': ['wordpress1.jpg', 'wordpress2.jpg', 'wordpress3.jpg', 'wordpress4.jpg', 'wordpress5.jpg',
        'wordpress6.jpg', 'wordpress7.jpg', 'wordpress8.jpg', 'wordpress9.jpg', 'wordpress10.jpg']}
};

let loadingImagesArray = findImages(imagesObject, 'all');
loadImages(findImages(imagesObject, 'all'));
createLoadButton();

let galleryContainer = document.querySelector('.amazing-work-gallery');
let selectTabsList = document.querySelector('.amazing-work-gallery-navigation');

selectTabsList.addEventListener('click', (event) => {
    let newActiveTab = event.target;

    if (newActiveTab !== selectTabsList && !newActiveTab.classList.contains('active-item-work'))  {

        let tabName = changeTabs(newActiveTab);
        galleryContainer.innerHTML = '';
        let tabImagesArray = findImages(imagesObject, tabName);
        loadingImagesArray = tabImagesArray;

        loadImages(tabImagesArray);
        removeLoadButton();

        if (tabImagesArray.length > 12) {
            createLoadButton();
        }
    }
});


function changeTabs(clickedTab) {
    let activeTab = document.querySelector('.active-item-work');

    activeTab.classList.remove('active-item-work');
    clickedTab.classList.add('active-item-work');
    clickedTab = clickedTab.innerText.toLowerCase();
    return clickedTab;
}

function findImages(object, tabName) {
    let findEqualKeys = Object.keys(object).find(key => key.toLowerCase() === tabName);

    if (findEqualKeys) {
        return getAllImages(object, tabName);
    } else {
        return getCategoryImage(object, tabName);
    }
}

function createLoadButton() {
    let amazingSection = document.querySelector('.our-amazing-work');
    // создадим кнопку - добавим элементу класс и текстовое значение
    let loadBtn = document.createElement('button');

    loadBtn.setAttribute('id', 'amazing-load');
    loadBtn.classList.add('btn-load-images');
    loadBtn.innerHTML = `<i class="fas fa-plus"></i><span style="margin-left: 15px">Load more</span>`;

    amazingSection.appendChild(loadBtn);
    loadBtnListener();
}

function removeLoadButton() {
    let checkLoadBtn = document.querySelector('#amazing-load');

    if (checkLoadBtn) {
        checkLoadBtn.remove();
    }
}

function loadBtnListener() {
    let counterLoadBtn = 0;
    let loadMoreButton = document.querySelector('#amazing-load');
    let loadWaiting = document.querySelector('#load-image');

    loadMoreButton.addEventListener('click', () => {
        counterLoadBtn += 1;
        if (counterLoadBtn === 2) {
            removeLoadButton();
        }
        loadMoreButton.hidden = true;
        loadWaiting.style.display = 'block';
        setTimeout(() => {
            loadMoreImages(loadingImagesArray);
            loadMoreButton.hidden = false;
            loadWaiting.style.display = 'none';
        }, 2000);
    });
}

function loadMoreImages(categoryImagesArray) {
    for (let i = 0; i <= 11; i++) {
        categoryImagesArray.shift();
    }
    if (categoryImagesArray.length <= 12) {
        removeLoadButton();
    }
    loadingImagesArray = categoryImagesArray;
    loadImages(loadingImagesArray);
}

function getAllImages(object, firstKey) {
    let ArrayOfAllImages = [];
    for(let key in object[firstKey]) {
        // noinspection JSUnfilteredForInLoop
        let tmpArr = object[firstKey][key];
        ArrayOfAllImages = ArrayOfAllImages.concat(tmpArr);
    }
    return ArrayOfAllImages;
}


function getCategoryImage(object, tabName) {
    let findKey = Object.keys(object).find(key => key.toLowerCase() === tabName);

    if (findKey) {
        return Array.from(object[findKey]);
    }
    for (let key in object) {
        if (typeof object[key] === 'object') {
            return getCategoryImage(object[key], tabName);
        }
    }
}

// фунция загрузки картинок
function loadImages(array) {
    let galleryGridClass = document.querySelector('.amazing-work-gallery');

    array.forEach((item, index) => {
        if (index <= 11) {
            let imageContainer = document.createElement('div');
            imageContainer.classList.add('gallery-image-container');

            let imageItem = document.createElement('img');
            imageItem.classList.add('gallery-image-size');
            imageItem.setAttribute('src', `./img/amazing-work-img/${item}`);

            let imageButtonsContainer = document.createElement('div');
            imageButtonsContainer.classList.add('gallery-image-btn-container');

            let imageLinkBtn = document.createElement('a');
            imageLinkBtn.setAttribute('href', '#');
            imageLinkBtn.classList.add('gallery-image-btn');

            let imageLinkIcon = document.createElement('i');
            imageLinkIcon.className = 'fas fa-link gallery-image-icons-prop';

            let imageSearchBtn = document.createElement('a');
            imageSearchBtn.setAttribute('href', '#');
            imageSearchBtn.classList.add('gallery-image-btn');

            let imageSearchIcon = document.createElement('i');
            imageSearchIcon.className = 'fas fa-search gallery-image-icons-prop';

            let imageTitle = document.createElement('p');
            imageTitle.classList.add('gallery-image-title');
            imageTitle.innerText = 'creative design';

            let imageSubtitle = document.createElement('p');
            imageSubtitle.classList.add('gallery-image-subtitle');
            imageSubtitle.innerText = 'Web Design';

            imageContainer.appendChild(imageItem);
            imageLinkBtn.appendChild(imageLinkIcon);
            imageSearchBtn.appendChild(imageSearchIcon);
            imageButtonsContainer.appendChild(imageLinkBtn);
            imageButtonsContainer.appendChild(imageSearchBtn);
            imageButtonsContainer.appendChild(imageTitle);
            imageButtonsContainer.appendChild(imageSubtitle);
            imageContainer.appendChild(imageButtonsContainer);

            imageContainer.style.display = 'none';
            $(imageContainer).delay(200*index).fadeIn(1000);
            galleryGridClass.appendChild(imageContainer);
        }
    });
}

// let gridClass = 'amazing-work-gallery';      // блок в котором находятся картинки
// let imagePath = 'img/amazing-work-img/'; // from root to images
// let newWork;
// newWork = new AmazingWork(imagesObject, gridClass, imagePath);
//
// function AmazingWork(object, galleryGridClass, imagePath) {
//     let amazingContainer = 'our-amazing-work';  // section amazing-work
//     let galleryNavigationClass = 'amazing-work-gallery-navigation';  // список влкадкок(меню) выбора категории картинок
//     let counter = 0;                            //  переменная для подсчета нажатой клавиши Load more
//
//     this.selectTab = function () {  // функция выбора влкадки
//
//         const activeItem = document.querySelector('.active-item-work'); // выбираем активную влкадку при старте загрузки страници
//         /* передаем в следующею функцию класс списка <ul="classname">: galleryNavigationClass = 'classname'
//         *  activeItem - активнуая влкадка на данный момент
//         *  object - обект с картинками
//         *   galleryGridClass - наш класс контейнер для картинок , куда будут добавлятся картинки */
//         this.changeTabs(galleryNavigationClass, activeItem, object, galleryGridClass); // функция изминения влкадки
//         this.createBtnAddImage(); // создаем кнопку load more в секции amazing-work при старте страници
//
//         const selectTabs = document.querySelector(`.${galleryNavigationClass}`);
//         selectTabs.addEventListener('click', (event) => {
//             let isActive = event.target;
//             // console.log(event.target);
//
//             this.changeTabs(galleryNavigationClass, isActive, imagesObject, galleryGridClass);
//             this.btnAddImage.remove();
//             counter = 0;
//             this.createBtnAddImage();
//         });
//     };
//
//     this.getAllImages = function (object, firstKey) {
//
//         let ArrayOfAllImages = [];
//         for(let key in object[firstKey]) {
//             // noinspection JSUnfilteredForInLoop
//             let tmpArr = object[firstKey][key];
//             // console.log(tmpArr);
//             ArrayOfAllImages = ArrayOfAllImages.concat(tmpArr);
//         }
//         return ArrayOfAllImages;
//     };
//
//     this.getCategoryImage = function (object, tabName) {
//         let findKey = Object.keys(object).find(key => key.toLowerCase() === tabName);
//         if (findKey) {
//             return Array.from(object[findKey]);
//         }
//         for (let key in object) {
//             if (object.hasOwnProperty(key)) {
//                 if (typeof object[key] === 'object') {
//                     return this.getCategoryImage(object[key], tabName);
//                 }
//             }
//         }
//     };
//
//
//     let fullArray =[];
//     this.changeTabs = function (galleryNavigationClass, checkIsActive, object, classGrid) {
//         // выделим элементы в которые будем перезаписывать значение текст и картинку
//         let tabContentGrid = document.querySelector(`.${classGrid}`);
//         // получаем наш лист <ul>
//         const selectedList = document.querySelector(`.${galleryNavigationClass}`);
//         // делаем из детей листа массив <ul> <li> </li> </ul> ;
//         let arrayTabs = Array.from(selectedList.children);
//         // присваиваем строку с классами в checkIsList
//         let checkIsList = selectedList.className;
//         //проверяем если нажата влкадка у которой уже есть класс 'active' то вернём тру
//         // console.log(checkIsList);
//         if (checkIsActive.classList.contains('active-item-work') && tabContentGrid.childElementCount !== 0) {
//             return true;
//             // если нажатая влкадка оказалась Листом <ul> то вернём фолс
//         } else if (checkIsActive.classList.contains(checkIsList)) {
//             return false;
//         } else {
//             // в любом другом случае должна быть нажата именно вкладка
//             // найдем активную вкалдку из массива элементов <li> у которой есть class="active';
//             let findActive = arrayTabs.find(item => item.classList.contains('active-item-work'));
//             // удалим класс 'active'
//             findActive.classList.remove('active-item-work');
//
//             // у нажатой влкадки возьмём текст и переведем его в нижний регистр и запишим в tabName
//             let tabName = checkIsActive.innerText.toLowerCase();
//             // теперь с помощью Object.keys() получим  массив включей нашего обьекта который мы передали
//             // сравним ключи которые есть в обьекте с названием вкладки и если таков есть запишим ключ в переменную
//             let findEqualKeys = Object.keys(object).find(key => key.toLowerCase() === tabName);
//             // console.log(findEqualKeys);
//             if (findEqualKeys) {
//                 // если ключ на первом уровне вложености то мы получаем всё что на следующем уровне
//                 // все обьекты с ключами на втором уровне обьеденятся в один массив
//                 fullArray = this.getAllImages(object, findEqualKeys);
//             }
//             else {
//                 // иначе зайдем на второй уровень вложености и найдем нужный ключ категории
//                 // и запишим результат-массив нужной категории в переменную
//                 fullArray = this.getCategoryImage(object, tabName);
//             }
//             // Добавим класс 'active' новой вкладке
//             checkIsActive.classList.toggle('active-item-work');
//
//             tabContentGrid.innerHTML = ''; // очистим контейнер с картинками
//             this.loadImages(fullArray, galleryGridClass); // вызовем функцию которая добавит картинки
//         }
//     };
//
//     this.createBtnAddImage = function () {
//         // получим секцию our-amazing-works
//         const container = document.querySelector(`.${amazingContainer}`);
//         // создадим кнопку - добавим элементу класс и текстовое значение
//         this.btnAddImage = document.createElement('button');
//         this.btnAddImage.classList.add('btn-load-images');
//         // let iconPlus = document.createElement('i');
//         // iconPlus.className = 'fas fa-plus';
//         this.btnAddImage.innerHTML = `<i class="fas fa-plus"></i><span style="margin-left: 15px">Load more</span>`;
//
//         let newArr = fullArray.map((item)=> item);
//         // получим блок в котором есть анимация загрузки
//         const loadingItem = document.querySelector('#load-image');
//
//         // вешаем событие на кнопку 'Load more'
//         this.btnAddImage.addEventListener('click', () => {
//
//             counter += 1; //counter чтобы после второго нажатия 'Load more' она исчезала
//             if (counter === 2) {
//                 counter = 0;
//                 this.btnAddImage.remove();
//             }
//
//             // циклом отсеем из массива первые 12 картинок которые изначально есть на странице
//             // чтобы не добавить их еще раз
//             for (let i = 0; i < 12; i++) {
//                 fullArray.shift();
//             }
//
//             // если длина полученого массива изначально больше чем 12
//             if (newArr.length >= 12) {
//                 // то сделаем анимацию загрузки видимой
//                 // а кнопку 'Load more' не видимой
//                 loadingItem.style.display = 'block';
//                 let btnAdd = this.btnAddImage;
//                 btnAdd.hidden = true;
//
//                 // делаем задержку в 2секунды и в этот момент есть анимаци загрузки
//                 // после выполнения добавятся картинки в контейнер
//                 // кнопка 'load more станет видимой, а анимация загрузки не видимой'
//                 setTimeout(() => {
//
//                     this.loadImages(fullArray, galleryGridClass);
//                     loadingItem.style.display = 'none';
//                     btnAdd.hidden = false;
//
//                 }, 2000)
//             }
//
//             /* если после первого нажатия на кнопку 'Load more'
//                 массив из которого отсеятся первые 12, и массив будет меньше чем 12 элементов
//                 то убёрем кнопку 'load more' , после первого нажатия
//              */
//             if (fullArray.length < 12) {
//                 this.btnAddImage.remove();
//             }
//         });
//         // если изначальный массив больше 12 элементов (у категорий) то мы добавим кнопку
//         // для возможности догрузить еще картинок 1 или 2 раза
//         if (newArr.length > 12) {
//             container.appendChild(this.btnAddImage);
//         }
//     };
//
//     // фунция загрузки картинок
//     this.loadImages = function (array, galleryGridClass) {
//         galleryGridClass = document.querySelector(`.${galleryGridClass}`);
//
//         array.forEach((item, index) => {
//             if (index <= 11) {
//                 let imageContainer = document.createElement('div');
//                 imageContainer.classList.add('gallery-image-container');
//
//                 let imageItem = document.createElement('img');
//                 imageItem.classList.add('gallery-image-size');
//                 imageItem.setAttribute('src', `./${imagePath}${item}`);
//
//                 let imageButtonsContainer = document.createElement('div');
//                 imageButtonsContainer.classList.add('gallery-image-btn-container');
//
//                 let imageLinkBtn = document.createElement('a');
//                 imageLinkBtn.setAttribute('href', '#');
//                 imageLinkBtn.classList.add('gallery-image-btn');
//
//                 let imageLinkIcon = document.createElement('i');
//                 imageLinkIcon.className = 'fas fa-link gallery-image-icons-prop';
//
//                 let imageSearchBtn = document.createElement('a');
//                 imageSearchBtn.setAttribute('href', '#');
//                 imageSearchBtn.classList.add('gallery-image-btn');
//
//                 let imageSearchIcon = document.createElement('i');
//                 imageSearchIcon.className = 'fas fa-search gallery-image-icons-prop';
//
//                 let imageTitle = document.createElement('p');
//                 imageTitle.classList.add('gallery-image-title');
//                 imageTitle.innerText = 'creative design';
//
//                 let imageSubtitle = document.createElement('p');
//                 imageSubtitle.classList.add('gallery-image-subtitle');
//                 imageSubtitle.innerText = 'Web Design';
//
//                 imageContainer.appendChild(imageItem);
//                 imageLinkBtn.appendChild(imageLinkIcon);
//                 imageSearchBtn.appendChild(imageSearchIcon);
//                 imageButtonsContainer.appendChild(imageLinkBtn);
//                 imageButtonsContainer.appendChild(imageSearchBtn);
//                 imageButtonsContainer.appendChild(imageTitle);
//                 imageButtonsContainer.appendChild(imageSubtitle);
//                 imageContainer.appendChild(imageButtonsContainer);
//
//                 imageContainer.style.display = 'none';
//                 $(imageContainer).delay(200*index).fadeIn(1000);
//                 galleryGridClass.appendChild(imageContainer);
//             }
//         });
//     };
//     this.selectTab();
// }